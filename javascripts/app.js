$(document).ready(function(){
    $('.slider-express-auction, .slider-auction, .slider-stock').slick({
        slidesToShow: 7,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                }
            }
        ]
    });

    $(".slick-slide").on("mouseenter", function() {
            clearTimeout($(this).data('timeoutId'));
            var p = $(this).find('p'),
                fullText = p.data('fullText'),
                offset = p.offset();

            if(fullText) {
                $('body')
                    .append("<p class='hover-slider-text'>" + fullText + "</p>")
                    .find(".hover-slider-text")
                    .css({'position': 'absolute', 'top': offset.top, 'left': offset.left, 'width': p.parent().find('img').width()})
                    .fadeIn("slow");
            };

            $(".hover-slider-text").on("mouseenter", function() {
                $(this).addClass("hover");
            });
        }).mouseleave(function() {
                var isHovered = $(".hover-slider-text.hover");
                if(!isHovered && isHovered === undefined) {
                    $(".hover-slider-text").remove();
                    var timeoutId = setTimeout(function(){
                        $(".hover-slider-text").fadeOut("slow");
                    }, 650);
                    $(this).data('timeoutId', timeoutId);
                }
    });

    //component-auction
    $(".btn-auction").on('click', function() {
        $(".is-show-hide-block").slideToggle();
    });

    $(".pop-image").on("click", function() {
        $('#imagepreview').attr('src', $(this).find('img').attr('data-full-size'));
        $('#imagepreview-tittle').text($(this).find('img').attr('alt'))
        $('#imagemodal').modal('show');
    });


});